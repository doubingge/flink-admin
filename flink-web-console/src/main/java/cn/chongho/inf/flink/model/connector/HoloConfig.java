package cn.chongho.inf.flink.model.connector;

import cn.chongho.inf.flink.constants.ConnectorConfig;
import lombok.Data;

/**
 * config sql for hologres.
 * @author  feihu.wang
 */
@Data
public class HoloConfig implements ConfigToSql{


    private final String connector = "hologres";

    private String endpoint;

    private String dbName;

    private String tableName;

    private String username;

    private String password;

    public HoloConfig() {

    }


    public HoloConfig(String endpoint, String dbName, String tableName, String username, String password) {
        this.endpoint = endpoint;
        this.dbName = dbName;
        this.tableName = tableName;
        this.username = username;
        this.password = password;
    }

    @Override
    public String doConfigToSql() {

        String configSql = "(" +
                "'" + ConnectorConfig.ConfigValue.CONNECTOR.getValue() + "'" +
                "='" + connector + "'," +
                "'" + ConnectorConfig.ConfigValue.ENDPOINT.getValue() + "'" +
                "='" + endpoint + "'," +
                "'" + ConnectorConfig.ConfigValue.DB_NAME.getValue() + "'" +
                "='" + dbName + "'," +
                "'" + ConnectorConfig.ConfigValue.TABLE_NAME.getValue() + "'" +
                "='" + tableName + "'," +
                "'" + ConnectorConfig.ConfigValue.USERNAME.getValue() + "'" +
                "='" + username + "'," +
                "'" + ConnectorConfig.ConfigValue.PASSWORD.getValue() + "'" +
                "='" + password + "'" +
                ")";

        return configSql;
    }
}
